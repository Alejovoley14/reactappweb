import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
 
const AnyReactComponent = ({ text }) => <div>{text}</div>;
 
class SimpleMap extends Component {
  static defaultProps = {
    center: {
      lat: 59.95,
      lng: 30.33
    },
    zoom: 11
  };
 
  render() {
    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '400px', width: '1300px', marginBottom: '40px' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: 'AIzaSyDepl9ghjxshRGCINDtJlNuqtQg61XMRhM'}}
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
        >
          <AnyReactComponent
            lat={-34.6183196}
            lng={-58.631205}
            text="Mi Marcador"
          />
        </GoogleMapReact>
      </div>
    );
  }
}
 
export default SimpleMap;