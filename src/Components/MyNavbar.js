import React from 'react';
import { Navbar, Form, Nav, Button, FormControl, NavDropdown } from 'react-bootstrap';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faUser } from '@fortawesome/free-solid-svg-icons'


class MyNavbar extends React.Component {

    render() {
        return (

            // React.createElement("nav", {class: "navbar navbar-expand-lg navbar-light bg-dark"},

            //     React.createElement("a", {class: "navbar-brand text-white", href: "#"}, "Form"),

            //     React.createElement("div", {class: "collapse navbar-collapse", id: "navbarNavAltMarkup"},

            //         React.createElement('div', {class: "navbar-nav"},

            //             React.createElement("a", {class: "nav-item nav-link active text-white", href: "#"}, "Home"),
            //             React.createElement("a", {class: "nav-item nav-link text-white", href: "#"}, "Category"),
            //             React.createElement("a", {class: "nav-item nav-link text-white", href: "#"}, "Products")

            //         )

            //     )
            // )

            <Navbar bg="dark" variant="dark">
                
                <Navbar.Brand href="#home"><img src="./Images/logo192.png" alt="img" width="50px" height="50px"></img></Navbar.Brand>

                <Nav className="mr-auto">
                    <Nav.Link href="#home">Home</Nav.Link>
                    <Nav.Link href="#features">Category</Nav.Link>
                    <Nav.Link href="#pricing">Products</Nav.Link>
                </Nav>

                <Form inline>
                    <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                    <Button variant="outline-info">Search</Button>
                </Form>

            </Navbar>

        )
    }

}

export default MyNavbar;
