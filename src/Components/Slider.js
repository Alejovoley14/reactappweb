import React from 'react';
import { Carousel } from 'react-bootstrap';

class Slider extends React.Component {

    render() {
        return (

            // React.createElement("div", { id: "carouselExampleControls", class: "carousel slide", dateride: "carousel" },

            //     React.createElement("div", { class: "carousel-inner" },

            //         React.createElement("div", { class: "carousel-item active" },

            //             React.createElement("img", { class: "d-block w-100", src: "./Images/Slider1.jpg", alt: "First Slide" })

            //         ),

            //         React.createElement("div", { class: "carousel-item" },

            //             React.createElement("img", { class: "d-block w-100", src: "./Images/Slider2.jpg", alt: "Second Slide" })

            //         ),

            //         React.createElement("div", { class: "carousel-item" },

            //             React.createElement("img", { class: "d-block w-100", src: "./Images/Slider3.jpg", alt: "First Slide" })

            //         )

            //     ),

            //     React.createElement("a", { class: "carousel-control-prev", href: "#carouselExampleIndicators", role: "button", dataslide: "prev" },

            //         React.createElement("span", { class: "carousel-control-prev-icon", ariahidden: "true" }),
            //         React.createElement("span", { class: "sr-only" }, "Previous"),

            //     ),

            //     React.createElement("a", { class: "carousel-control-next", href: "#carouselExampleIndicators", role: "button", dataslide: "next" },

            //         React.createElement("span", { class: "carousel-control-next-icon", ariahidden: "true" }),
            //         React.createElement("span", { class: "sr-only" }, "Next"),

            //     )

            // )

            // Forma de hacerlo con React createElement

            <Carousel>

                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="./Images/Slider1.jpg"
                        alt="First slide"
                    />
                    <Carousel.Caption>
                        <h3>First slide label</h3>
                        <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                    </Carousel.Caption>
                </Carousel.Item>

                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="./Images/Slider2.jpg"
                        alt="Third slide"
                    />

                    <Carousel.Caption>
                        <h3>Second slide label</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </Carousel.Caption>
                </Carousel.Item>

                <Carousel.Item>
                    <img
                        className="d-block w-100"
                        src="./Images/Slider3.jpg"
                        alt="Third slide"
                    />

                    <Carousel.Caption>
                        <h3>Third slide label</h3>
                        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
                    </Carousel.Caption>

                </Carousel.Item>

            </Carousel>


        )
    }

}

export default Slider;
