import React from 'react';

class Form extends React.Component {

    render() {
        return (

            React.createElement('form', {},

                React.createElement('div', { class: "form-group w-75" },

                    React.createElement('label', {}, "First and Last Name"),
                    React.createElement('input', { type: "text", class: "form-control mb-2", placeholder: "Insert your First and Last Name" }),

                    React.createElement('label', {}, "UserName"),
                    React.createElement('input', { type: "text", class: "form-control mb-2", placeholder: "Insert your Username" }),

                    React.createElement('label', {}, "Password"),
                    React.createElement('input', { type: "password", class: "form-control mb-2", placeholder: "Insert your Password" }),

                    React.createElement('input', { type: "submit", class: "btn btn-primary", value: "Submit" }),

                )

            )
        )
    }

}

export default Form;
