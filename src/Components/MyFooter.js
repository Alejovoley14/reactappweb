import React from 'react';

class MyFooter extends React.Component {

    render() {
        return (

            <footer class="footer mt-4 bg-dark">

                <div class="footer-copyright text-center py-3 text-white">© 2020 Copyright
                  <a href="#hola" class="ml-2">React App Web </a>
                </div>

            </footer>
        )
    }

}

export default MyFooter;