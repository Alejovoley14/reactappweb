import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root')); // Aca se indica que componente sera el que el arbol del DOM renderice

serviceWorker.unregister();
