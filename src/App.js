import React from 'react';
import MyNavbar from './Components/MyNavbar';
import Slider from './Components/Slider';
import MyCard from './Components/MyCard';
import MyFooter from './Components/MyFooter';
import 'bootstrap/dist/css/bootstrap.css';


function App() {

  return (

    <div>
      <MyNavbar></MyNavbar>
      <Slider></Slider>

      <div class="container">

        <h1 class="text-center my-3">Cards</h1>
        <div class="row mt-4">

          <div class="col-sm">
            <MyCard image="./Images/Card1.jpg"></MyCard>
          </div>

          <div class="col-sm">
            <MyCard image="./Images/Card2.jpg"></MyCard>
          </div>

          <div class="col-sm">
            <MyCard image="./Images/Card3.jpg"></MyCard>
          </div>

        </div>
      </div>

      <MyFooter></MyFooter>

    </div>

  );
}

export default App;
